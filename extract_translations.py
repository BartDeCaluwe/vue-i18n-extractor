import json
import os
import re

def main():
    # TODO: Parse paths from arguments
    folder = './src/components/'
    localizationfolder = './src/locales/'

    # Create a set so we have unique string values
    translation_set = set([])

    for filename in os.listdir(folder):
        with open(folder + filename, "r") as f:
            for line in f:
                if re.search('\$t\("',line): 
                    start = line.find('$t("')
                    end = line.find('")')
                    translation_set.add(line[start + 4:end])
                    continue
                if re.search("\$t\('",line): 
                    start = line.find("$t('")
                    end = line.find("')")
                    translation_set.add(line[start + 4:end])

    sorted_translations = sorted(translation_set)
    translations = dict.fromkeys(sorted_translations, None)

    # Loop over localization files
    for filename in os.listdir(localizationfolder):
        filedata = {}
        with open(filename, 'r') as file:
            try:
                filedata = json.load(file)
            except json.decoder.JSONDecodeError as error:
                print('Failed to decode file: ' + error.msg)
                sys.exit()

            for key in translations:
                try:
                    # Skip existing translation key
                    if filedata[key]:
                        continue
                # Add missing translation key
                except KeyError as error:
                    filedata[key] = None
                    continue

        with open(filename, 'w') as file:
            json.dump(filedata, file, indent=2, ensure_ascii=False)
            file.write('\n')


if __name__ == "__main__":
    main()
